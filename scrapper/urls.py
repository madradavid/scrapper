from django.conf.urls import patterns, url
from scrapper import settings
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       url(r'^$', 'scrapper.views.home_page', name='home'),
                       url(r'^inspections/(?P<restaurant>[^\.]+)/$',
                           'scrapper.views.inspections_page', name='inspections'),
                       url(r'^violations/(?P<inspection>[^\.]+)/$',
                           'scrapper.views.violations_page', name='violations'),
                       # url(r'^scrapper/', include('scrapper.foo.urls')),

                       # Uncomment the admin/doc line below to enable admin documentation:
                       # url(r'^admin/doc/',
                       # include('django.contrib.admindocs.urls')),

                       # Uncomment the next line to enable the admin:
                       # url(r'^admin/', include(admin.site.urls)),
                       )
# localhost urls
if settings.LOCALHOST:
    urlpatterns += patterns('',
                            url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
                                {'document_root': settings.STATIC_ROOT,
                                 'show_indexes': False}

                                ),
                            )
