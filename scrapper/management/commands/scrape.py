from django.core.management.base import BaseCommand, CommandError
from scrapper import scrapperUtils


class Command(BaseCommand):
    args = '<query...>'
    help = 'Scrape http://health.state.tn.us/EHInspections/ for new inspections and Populate database'

    def handle(self, *args, **options):
        if len(args) != 1:
            raise CommandError("Scrape requires a Search Value")
        scrapperUtils.scrape(args)
