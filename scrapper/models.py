from django.db import models
from datetime import datetime


class Restaurant(models.Model):
    name = models.CharField(max_length=256)
    uid_number = models.CharField(max_length=256, unique=True)
    added = models.DateTimeField(auto_now_add=True)
    address = models.TextField(blank=True, null=True)
    city = models.CharField(max_length=256)
    last_score = models.IntegerField(max_length=10, null=True, blank=True)


class Inspection(models.Model):
    restaurant = models.ForeignKey(Restaurant)
    uid_number = models.CharField(max_length=256, unique=True)
    date = models.DateTimeField(default=datetime.now, blank=True)
    repeated_violations = models.CharField(max_length=256)
    critical_violations = models.CharField(max_length=256)
    score = models.IntegerField(max_length=10, null=True, blank=True)
    inspection_type = models.TextField(blank=True, null=True)


class Violation(models.Model):
    inspection = models.ForeignKey(Inspection)
    uid_number = models.CharField(max_length=256, unique=True)
    date = models.DateTimeField(default=datetime.now, blank=True)
    description = models.TextField(blank=True, null=True)
    score = models.IntegerField(max_length=10, null=True, blank=True)
