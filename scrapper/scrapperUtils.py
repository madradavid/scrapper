import urllib
import json
from scrapper.models import Restaurant, Inspection, Violation


def scrape(arg):
    url = 'http://health.state.tn.us/EHInspections/EHInspections/GetSearchResults?ProgramID=0605&SearchTerms=%s&rows=50' % (
        arg[0])
    print "Fetching Restaurants...."
    data = fetch_data(url)
    if 'rows' in data:
        for d in data['rows']:
            add_restaurant(d)


def fetch_data(url):
    data = False
    f = urllib.urlopen(url)
    [content, response_code] = (f.read(), f.code)
    if(response_code == 200):
        data = json.loads(str(content))
    else:
        return "Server returned with Error %s" % response_code
    return data


def add_restaurant(data):
    try:
        restaurant, created = Restaurant.objects.get_or_create(
            uid_number=data['i'])
        if created:
            restaurant.name = data['cell'][1]
            restaurant.address = "%s" % data['cell'][2]
            restaurant.city = "%s" % data['cell'][3]
            restaurant.score = data['cell'][3]
            restaurant.save()
        add_inspection(restaurant)
    except Exception, e:
        print e


def add_inspection(restaurant):
    """
    Fetch the inspection data
    the LicenseNbr is the uid_number minus the first 4 digits or ProgramID value
    """
    url = "http://health.state.tn.us/EHInspections/EHInspections/GetInspections?ProgramID=0605&LicenseNbr=%s&_search=false&nd=1403084900695&rows=20" % (
        restaurant.uid_number[4:])
    print "Fetching Inspections...."
    data = fetch_data(url)
    if 'rows' in data:
        for d in data['rows']:
            try:
                inspection, created = Inspection.objects.get_or_create(
                    uid_number=d['i'], restaurant=restaurant)
                if created:
                    inspection.repeated_violations = d['cell'][4]
                    inspection.critical_violations = d['cell'][5]
                    inspection.inspection_type = d['cell'][2]
                    inspection.save()
                    # add violations for every inspection
                    add_violation(inspection)
            except Exception, e:
                print e


def add_violation(inspection):
    url = "http://health.state.tn.us/EHInspections/EHInspections/GetViolations?InspectionID=%s&LicenseNbr=0605219652&_search=false&nd=1403085189467&rows=20&page=1&sidx=num&sord=asc" % (
        inspection.uid_number)
    print "Fetching Violations...."
    data = fetch_data(url)
    if 'rows' in data:
        for d in data['rows']:
            try:
                violation, created = Violation.objects.get_or_create(
                    uid_number=d['i'], inspection=inspection)
                if created:
                    violation.description = data['cell'][0]
                    violation.save()
            except Exception, e:
                print e
