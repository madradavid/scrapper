# Create your views here.
from django.template import RequestContext
from django.shortcuts import render_to_response
from scrapper.models import Restaurant, Inspection, Violation
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from scrapper import settings


def render_view(request, template, data):
    '''
    wrapper for rendering views , loads RequestContext
    @request  request object
    @template  string
    @data  tumple
    '''
    return render_to_response(
        template, data,
        context_instance=RequestContext(request)
    )


def home_page(request):
    '''
    handles the landing page
    @request  request object
    '''
    paginator = Paginator(Restaurant.objects.all(), settings.PAGNATION_LIMIT)
    page = request.GET.get('page')
    try:
        restaurants = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        restaurants = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of
        # results.
        restaurants = paginator.page(paginator.num_pages)
    return render_view(request, 'index.html', {'restaurants': restaurants})


def inspections_page(request, restaurant):
    '''
    handles the landing page
    @request  request object
    '''
    paginator = Paginator(
        Inspection.objects.all().filter(restaurant=restaurant), settings.PAGNATION_LIMIT)
    page = request.GET.get('page')
    try:
        inspections = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        inspections = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of
        # results.
        inspections = paginator.page(paginator.num_pages)
    return render_view(request, 'inspections.html', {'inspections': inspections, 'page_descritpion': 'Inspections'})


def violations_page(request, inspection):
    '''
    handles the landing page
    @request  request object
    '''
    paginator = Paginator(
        Violation.objects.all().filter(inspection=inspection), settings.PAGNATION_LIMIT)
    page = request.GET.get('page')
    try:
        violations = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        violations = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of
        # results.
        violations = paginator.page(paginator.num_pages)
    return render_view(request, 'violations.html', {'violations': violations, 'page_descritpion': 'Violations'})